<?php
namespace army;
/**
 * Created by PhpStorm.
 * User: Extention
 * Date: 09.11.2016
 * Time: 14:08
 */
class AbstractUnit implements \Unit
{
    private $attack;
    private $defence;
    public function __construct($attack, $defence){
        $this->attack = $attack;
        $this->defence = $defence;
    }
    public function attack(){
        return $this->attack;
    }
    public function defence(){
        return $this->defence;
    }
}