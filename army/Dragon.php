<?php
/**
 * Created by PhpStorm.
 * User: Extention
 * Date: 09.11.2016
 * Time: 14:46
 */

namespace army;


class Dragon extends AbstractUnit
{
    use Flying;
    public function __construct(){
        parent::__construct(10, 10);
    }
}