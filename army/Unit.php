<?php

/**
 * Created by PhpStorm.
 * User: Extention
 * Date: 09.11.2016
 * Time: 14:05
 */
interface Unit
{
    public function attack();
    public function defence();
}